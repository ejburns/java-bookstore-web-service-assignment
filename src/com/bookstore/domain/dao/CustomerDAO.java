package com.bookstore.domain.dao;

import java.util.List;

import com.bookstore.domain.model.Address;
import com.bookstore.domain.model.Book;
import com.bookstore.domain.model.CreditCard;
import com.bookstore.domain.model.Customer;
import com.bookstore.tests.model.MockCustomerTable;

public class CustomerDAO {
	
	private static MockCustomerTable db = new MockCustomerTable();
	
	/**
	 * Returns all the customers
	 * @return
	 */
	public List<Customer> getCustomers() {
		return db.customers;
	}
	
	/**
	 * Returns a specific customer, based on ID.
	 * @param id
	 * @return
	 */
	public Customer getCustomer(String id) {
		for (Customer customer : db.customers) {
			if(customer.getId().equals(id)) {
				return customer;
			}
		}
		return null;
	}
	
	/**
	 * Add a new customer to the database.
	 * @param firstName
	 * @param lastName
	 * @param addresses
	 * @param creditCards
	 * @return
	 */
	public Customer addCustomer(String firstName, String lastName, List<Address> addresses,
								List<CreditCard> creditCards, List<Book> books) {
		
		// Create the new book.
		Customer customer = new Customer();
		customer.setId(String.valueOf(db.customers.size()));
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAddresses(addresses);
		customer.setCreditCards(creditCards);
		customer.setBooks(books);
		
		// Add the new book.
		db.customers.add(customer);
		
		// Return the new book.
		return customer;
	}

	/**
	 * Update a specific customer, based on ID.
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param addresses
	 * @param creditCards
	 */
	public void updateCustomer(String id, String firstName, String lastName, List<Address> addresses, 
							   List<CreditCard> creditCards, List<Book> books) {
		
		Customer customer = db.customers.get(Integer.parseInt(id));
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAddresses(addresses);
		customer.setCreditCards(creditCards);
		customer.setBooks(books);
	}
	
	/**
	 * Delete the customer with the given id.
	 * @param id
	 */
	public void deleteCustomer(String id) {
		db.customers.remove(Integer.parseInt(id));
	}
}
