package com.bookstore.domain.dao;

import java.util.List;

import com.bookstore.domain.model.Address;
import com.bookstore.tests.model.MockAddressTable;

public class AddressDAO {

	private static MockAddressTable db = new MockAddressTable();
	
	/**
	 * Return all of the addresses.
	 * @return
	 */
	public List<Address> getAddresses() {
		return db.addresses;
	}
	
	/**
	 * Returns a specific address.
	 * @param id
	 * @return
	 */
	public Address getAddress(String id) {
		for (Address address : db.addresses) {
			if(address.getId().equals(id)) {
				return address;
			}
		}
		return null;
	}
	
	/**
	 * Add a new address to the database.
	 * @param firstName
	 * @param lastName
	 * @param phone
	 * @param email
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 * @return
	 */
	public Address addAddress(String firstName, String lastName,
							  String phone, String email,
							  String address1, String address2,
							  String city, String state) {
		
		// Create the address
		Address address = new Address();
		address.setId(String.valueOf(db.addresses.size()));
		address.setFirstName(firstName);
		address.setLastName(lastName);
		address.setPhone(phone);
		address.setEmail(email);
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCity(city);
		address.setState(state);
		
		// Add the new address
		db.addresses.add(address);
		
		// Return the address
		return address;
	}
	
	/**
	 * Update an address in the database.
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param phone
	 * @param email
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 */
	public void updateAddress(String id, String firstName, String lastName,
								 String phone, String email,
								 String address1, String address2,
								 String city, String state) {

		// Create the address
		Address address = db.addresses.get(Integer.parseInt(id));
		address.setId(String.valueOf(db.addresses.size() -1));
		address.setFirstName(firstName);
		address.setLastName(lastName);
		address.setPhone(phone);
		address.setEmail(email);
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCity(city);
		address.setState(state);
	}
	
	/**
	 * Delete an address from the database.
	 * @param id
	 */
	public void deleteAddress(String id) {
		db.addresses.remove(Integer.parseInt(id));
	}
}
