package com.bookstore.domain.dao;

import java.util.List;

import com.bookstore.domain.model.ShippingCompany;
import com.bookstore.tests.model.MockShippingCompanyTable;

public class ShippingCompanyDAO {
	
	private static MockShippingCompanyTable msct = new MockShippingCompanyTable();

	public List<ShippingCompany> getShippingCompanies() {
		return msct.shippingCompanies;
	}
}
