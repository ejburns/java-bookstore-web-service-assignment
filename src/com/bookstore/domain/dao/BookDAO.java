package com.bookstore.domain.dao;

import java.util.List;

import com.bookstore.domain.model.Book;
import com.bookstore.tests.model.MockBookTable;

public class BookDAO {
	
	private static MockBookTable db = new MockBookTable();
	
	/**
	 * Returns all of the books.
	 * @return
	 */
	public List<Book> getBooks() {
		return db.books;
	}
	
	/**
	 * Returns a specific book, based on ID.
	 * 	If nothing is found, null is returned.
	 * @param id
	 * @return
	 */
	public Book getBook(String id) {
		for (Book book : db.books) {
			if(book.getId().equals(id)) {
				return book;
			}
		}
		return null;
	}
	
	/**
	 * Adds a new book to the database.
	 * @param isbn
	 * @param title
	 * @param author
	 * @param price
	 * @return
	 */
	public Book addBook(String isbn, String title, String author, double price) {
		
		// Create the new book.
		Book book = new Book();
		book.setId(String.valueOf(db.books.size()));
		book.setISBN(isbn);
		book.setTitle(title);
		book.setAuthor(author);
		book.setPrice(price);
		
		// Add the new book.
		db.books.add(book);
		
		// Return the new book.
		return book;
	}
	
	/**
	 * Updates a book with the passed information.
	 * @param id
	 * @param isbn
	 * @param title
	 * @param author
	 * @param price
	 */
	public void updateBook(String id, String isbn, String title, String author, double price) {
		Book book = db.books.get(Integer.parseInt(id));
		book.setISBN(isbn);
		book.setTitle(title);
		book.setAuthor(author);
		book.setPrice(price);
	}
	
	/**
	 * Deletes the book with the passed ID.
	 * @param id
	 */
	public void deleteBook(String id) {
		db.books.remove(Integer.parseInt(id));
	}

}
