package com.bookstore.domain.dao;

import java.util.ArrayList;
import java.util.List;

import com.bookstore.domain.model.Address;
import com.bookstore.domain.model.CreditCard;
import com.bookstore.domain.model.Customer;
import com.bookstore.domain.model.Line;
import com.bookstore.domain.model.Order;
import com.bookstore.domain.model.ShippingCompany;
import com.bookstore.tests.model.MockOrderTable;

public class OrderDAO {
	
	private static MockOrderTable db = new MockOrderTable();
	
	/**
	 * Return all of the orders.
	 * @return
	 */
	public List<Order> getOrders() {
		return db.orders;
	}
	
	/**
	 * Gets all the orders for a specific customer
	 * @param id
	 * @return
	 */
	public List<Order> getOrdersByCustomerId(String customerId) {
		List<Order> customerOrders = new ArrayList<Order>();
		for (Order order : db.orders){
			if (order.getCustomer().getId().equals(customerId)) {
				customerOrders.add(order);
			}
		}
		return customerOrders;
	}
	
	/**
	 * Return a specific order with the given ID.
	 * @param id
	 * @return
	 */
	public Order getOrder(String id) {
		for (Order order : db.orders) {
			if(order.getId().equals(id)) {
				return order;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param id
	 * @param customerId
	 * @return
	 */
	public Order getCustomerOrder(String id, String customerId) {
		List<Order> orders = getOrdersByCustomerId(customerId);
		for (Order order : orders) {
			if(order.getId().equals(id)) {
				return order;
			}
		}
		return null;
	}

	/**
	 * Add a new order to the database.
	 * @param customer
	 * @param billingAddress
	 * @param shippingAddress
	 * @param creditCard
	 * @param shippingCompany
	 * @param paymentReceived
	 * @return
	 */
	public Order addOrder(Customer customer, Address billingAddress, Address shippingAddress,
						  CreditCard creditCard, ShippingCompany shippingCompany, List<Line> lines, boolean paymentReceived,
						  String orderState) {
		
		// Create the new Order
		Order order = new Order();
		order.setId(String.valueOf(db.orders.size()));
		order.setCustomer(customer);
		order.setBillingAddress(billingAddress);
		order.setShippingAddress(shippingAddress);
		order.setCreditCard(creditCard);
		order.setShippingCompany(shippingCompany);
		order.setLines(lines);
		order.setPaymentReceived(paymentReceived);
		order.setOrderState(orderState);
		
		// Add the new order
		db.orders.add(order);
		
		// Return the newly created order
		return order;
	}
	
	/**
	 * Update the order with the specified id.
	 * @param id
	 * @param customer
	 * @param billingAddress
	 * @param shippingAddress
	 * @param creditCard
	 * @param shippingCompany
	 * @param paymentReceived
	 */
	public void updateCustomer(String id, Customer customer, Address billingAddress, Address shippingAddress,
						  CreditCard creditCard, ShippingCompany shippingCompany, boolean paymentReceived) {
		
		Order order = db.orders.get(Integer.parseInt(id));
		order.setId(String.valueOf(db.orders.size() -1));
		order.setCustomer(customer);
		order.setBillingAddress(billingAddress);
		order.setShippingAddress(shippingAddress);
		order.setCreditCard(creditCard);
		order.setShippingCompany(shippingCompany);
		order.setPaymentReceived(paymentReceived);
	}
	
	/**
	 * 
	 * @param customerId
	 * @return
	 */
	public Order getCustomerCurrentOrder(String customerId) {
		for (Order order : getOrdersByCustomerId(customerId)) {
			if (order.getOrderState().equals("Open")) {
				return order;
			}
		}
		return null;
	}
	
	/**
	 * Delete the order at the specified id.
	 * @param id
	 */
	public void deleteOrder(String id) {
		db.orders.remove(Integer.parseInt(id));
	}
}
