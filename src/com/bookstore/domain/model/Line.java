package com.bookstore.domain.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Line")
public class Line implements Serializable {
	
	private Book book;
	private int quantity;

	public Line() {}
	
	public Line(Book book, int quantity) {
		this.book = book;
		this.quantity = quantity;
	}
	
	public Book getBook() {
		return book;
	}
	
	public void setBook(Book book) {
		this.book = book;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	private static final long serialVersionUID = 1573496132336092706L;
}
