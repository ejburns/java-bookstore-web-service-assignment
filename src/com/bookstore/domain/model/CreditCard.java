package com.bookstore.domain.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CreditCard")
public class CreditCard implements Serializable {

	private String id;
	private String creditCardNumber;
	private String expirationDate;
	private String verificationCode;
	private String cardholderName;
	
	public CreditCard() {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	
	public String getExpirationDate() {
		return expirationDate;
	}
	
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public String getVerificationCode() {
		return verificationCode;
	}
	
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	
	public String getCardholderName() {
		return cardholderName;
	}
	
	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}
	
	private static final long serialVersionUID = 2394458124825411808L;	
}
