package com.bookstore.domain.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ShippingCompany")
public class ShippingCompany implements Serializable {
	
	private String id;
	private String companyName;
	private List<Address> locations;
	private double shippingRate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public List<Address> getLocations() {
		return locations;
	}
	public void setLocations(List<Address> locations) {
		this.locations = locations;
	}
	public double getShippingRate() {
		return shippingRate;
	}
	public void setShippingRate(double shippingRate) {
		this.shippingRate = shippingRate;
	}

	private static final long serialVersionUID = -2377526269509004484L;
}
