package com.bookstore.tests.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bookstore.domain.model.Address;

public class MockAddressTable {

	public List<Address> addresses;
	
	public MockAddressTable() {
		
		// Initialize the list
		addresses = new ArrayList<Address>();
		
		// Add the fake data
		Address address1 = new Address();
		address1.setId("0");
		address1.setFirstName("Eric");
		address1.setLastName("Burns");
		address1.setPhone("(312) 569 - 0528");
		address1.setEmail("ericjosephburns@gmail.com");
		address1.setAddress1("2670 N Burling St.");
		address1.setAddress2("Apt. 1");
		address1.setCity("Chicago");
		address1.setState("IL");
		address1.setZip("60614");

		Address address2 = new Address();
		address2.setId("1");
		address2.setFirstName("Leroy");
		address2.setLastName("Jenkins");
		address2.setPhone("(555) 555 - 5555");
		address2.setEmail("leroyjenkins@gmail.com");
		address2.setAddress1("1234 WoW St.");
		address2.setAddress2("Apt. 4A");
		address2.setCity("Chicago");
		address2.setState("IL");
		address2.setZip("60606");
		
		Address address3 = new Address();
		address3.setId("2");
		address3.setFirstName("Thomas");
		address3.setLastName("Cobal");
		address3.setPhone("(777) 777 - 7777");
		address3.setEmail("thomascobal@gmail.com");
		address3.setAddress1("5678 Condonal St.");
		address3.setAddress2("");
		address3.setCity("Dekalb");
		address3.setState("IL");
		address3.setZip("55555");
		
		Address address4 = new Address();
		address4.setId("3");
		address4.setFirstName("UPS");
		address4.setLastName("");
		address4.setPhone("(777) 444 - 5555");
		address4.setEmail("shipping@ups.com");
		address4.setAddress1("2214 UPS Ave.");
		address4.setAddress2("PO BOX 242");
		address4.setCity("Chicago");
		address4.setState("IL");
		address4.setZip("60606");
		
		addresses.addAll(Arrays.asList(address1, address2, address3, address4));
	}
}
