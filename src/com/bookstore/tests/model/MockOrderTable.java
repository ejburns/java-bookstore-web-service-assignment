package com.bookstore.tests.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bookstore.domain.model.Order;

public class MockOrderTable {

	public List<Order> orders;

	private MockCustomerTable mct = new MockCustomerTable();
	private MockBookTable mbt = new MockBookTable();
	
	public MockOrderTable() {
		
		// Initialize the list
		orders = new ArrayList<Order>();
		
		// Set up the fake data
		
		// ORDER 1
		Order order1 = new Order();
		order1.setId("0");
		order1.setCustomer(mct.customers.get(0));
		order1.setBillingAddress(mct.customers.get(0).getAddresses().get(0));
		order1.setShippingAddress(mct.customers.get(0).getAddresses().get(0));
		order1.setCreditCard(mct.customers.get(0).getCreditCards().get(0));
		order1.addBook(mbt.books.get(0), 2);
		order1.addBook(mbt.books.get(3), 1);
		order1.addBook(mbt.books.get(8), 1);
		
		orders.addAll(Arrays.asList(order1));
	}
}
