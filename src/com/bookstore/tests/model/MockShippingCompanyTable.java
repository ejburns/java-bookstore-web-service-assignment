package com.bookstore.tests.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bookstore.domain.model.Address;
import com.bookstore.domain.model.ShippingCompany;

public class MockShippingCompanyTable {
	
	public List<ShippingCompany> shippingCompanies;
	
	private MockAddressTable mat = new MockAddressTable();
	
	public MockShippingCompanyTable() {
		
		shippingCompanies = new ArrayList<ShippingCompany>();
		
		ShippingCompany shippingCompany1 = new ShippingCompany();
		shippingCompany1.setId("0");
		shippingCompany1.setCompanyName("UPS");
		List<Address> locations = new ArrayList<Address>();
		locations.add(mat.addresses.get(3));
		shippingCompany1.setLocations(locations);
		shippingCompany1.setShippingRate(5.49);
		
		shippingCompanies.addAll(Arrays.asList(shippingCompany1));
	}

}
