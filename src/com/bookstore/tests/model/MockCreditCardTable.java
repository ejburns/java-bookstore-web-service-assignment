package com.bookstore.tests.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bookstore.domain.model.CreditCard;

public class MockCreditCardTable {
	
	public List<CreditCard> creditCards;
	
	public MockCreditCardTable() {
		
		creditCards = new ArrayList<CreditCard>();
		
		// Add fake data
		CreditCard creditCard1 = new CreditCard();
		creditCard1.setId("0");
		creditCard1.setCreditCardNumber("1234123412341234");
		creditCard1.setExpirationDate("10/13");
		creditCard1.setVerificationCode("555");
		creditCard1.setCardholderName("Eric J Burns");
		
		CreditCard creditCard2 = new CreditCard();
		creditCard2.setId("1");
		creditCard2.setCreditCardNumber("1111222233334444");
		creditCard2.setExpirationDate("11/14");
		creditCard2.setVerificationCode("777");
		creditCard2.setCardholderName("Eric Burns");
		
		CreditCard creditCard3 = new CreditCard();
		creditCard3.setId("2");
		creditCard3.setCreditCardNumber("4444555566667777");
		creditCard3.setExpirationDate("3/15");
		creditCard3.setVerificationCode("111");
		creditCard3.setCardholderName("Thomas Cobal");
		
		CreditCard creditCard4 = new CreditCard();
		creditCard4.setId("3");
		creditCard4.setCreditCardNumber("8888999911112222");
		creditCard4.setExpirationDate("5/14");
		creditCard4.setVerificationCode("222");
		creditCard4.setCardholderName("Leroy Jenkins");
		
		creditCards.addAll(Arrays.asList(creditCard1, creditCard2, creditCard3, creditCard4));
	}
}
