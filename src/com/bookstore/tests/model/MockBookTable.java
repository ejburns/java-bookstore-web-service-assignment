package com.bookstore.tests.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bookstore.domain.model.Book;

public class MockBookTable {

	public List<Book> books;
	
	public MockBookTable() {
		// Initialize the list
		books = new ArrayList<Book>();
		
		// Set up fake data
		Book book1 = new Book();
		book1.setId("0");
		book1.setISBN("111111111");
		book1.setTitle("Book 1");
		book1.setAuthor("Book 1 Author");
		book1.setPrice(9.99);
		
		Book book2 = new Book();
		book2.setId("1");
		book2.setISBN("222222222");
		book2.setTitle("Book 2");
		book2.setAuthor("Book 2 Author");
		book2.setPrice(19.99);
		
		Book book3 = new Book();
		book3.setId("2");
		book3.setISBN("333333333");
		book3.setTitle("Book 3");
		book3.setAuthor("Book 3 Author");
		book3.setPrice(29.99);
		
		Book book4 = new Book();
		book4.setId("3");
		book4.setISBN("444444444");
		book4.setTitle("Book 4");
		book4.setAuthor("Book 4 Author");
		book4.setPrice(39.99);
		
		Book book5 = new Book();
		book5.setId("4");
		book5.setISBN("555555555");
		book5.setTitle("Book 5");
		book5.setAuthor("Book 5 Author");
		book5.setPrice(49.99);
		
		Book book6 = new Book();
		book6.setId("5");
		book6.setISBN("666666666");
		book6.setTitle("Book 6");
		book6.setAuthor("Book 6 Author");
		book6.setPrice(59.99);
		
		Book book7 = new Book();
		book7.setId("6");
		book7.setISBN("777777777");
		book7.setTitle("Book 7");
		book7.setAuthor("Book 7 Author");
		book7.setPrice(69.99);
		
		Book book8 = new Book();
		book8.setId("7");
		book8.setISBN("888888888");
		book8.setTitle("Book 8");
		book8.setAuthor("Book 8 Author");
		book8.setPrice(79.99);
		
		Book book9 = new Book();
		book9.setId("8");
		book9.setISBN("999999999");
		book9.setTitle("Book 9");
		book9.setAuthor("Book 9 Author");
		book9.setPrice(89.99);
		
		books.addAll(Arrays.asList(book1, book2, book3, book4, book5, book6, book7, book8, book9));
	}
}
