package com.bookstore.tests.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bookstore.domain.model.Address;
import com.bookstore.domain.model.Book;
import com.bookstore.domain.model.CreditCard;
import com.bookstore.domain.model.Customer;

public class MockCustomerTable {
	
	public List<Customer> customers;
	
	private MockAddressTable mat = new MockAddressTable();
	private MockCreditCardTable mcct = new MockCreditCardTable();
	private MockBookTable mbt = new MockBookTable();
	
	public MockCustomerTable() {
		
		// Initialize the fake table
		customers = new ArrayList<Customer>();
		
		// Add the fake data
		
		// CUSTOMER 1
		Customer customer1 = new Customer();
		customer1.setId("0");
		customer1.setFirstName("Eric");
		customer1.setLastName("Burns");
		
		customer1.setUsername("ericburns");
		customer1.setPassword("password");
		
		List<Address> c1a = new ArrayList<Address>();
		c1a.add(mat.addresses.get(0));
		customer1.setAddresses(c1a);
		
		List<CreditCard> c1cc = new ArrayList<CreditCard>();
		c1cc.add(mcct.creditCards.get(0));
		c1cc.add(mcct.creditCards.get(1));
		customer1.setCreditCards(c1cc);
		
		List<Book> c1b = new ArrayList<Book>();
		c1b.add(mbt.books.get(0));
		c1b.add(mbt.books.get(1));
		c1b.add(mbt.books.get(2));
		customer1.setBooks(c1b);
		
		// CUSTOMER 2
		Customer customer2 = new Customer();
		customer2.setId("1");
		customer2.setFirstName("Leroy");
		customer2.setLastName("Jenkins");
		
		customer2.setUsername("ljenkins");
		customer2.setPassword("password");
		
		List<Address> c2a = new ArrayList<Address>();
		c2a.add(mat.addresses.get(1));
		customer2.setAddresses(c2a);
		
		List<CreditCard> c2cc = new ArrayList<CreditCard>();
		c2cc.add(mcct.creditCards.get(3));
		customer2.setCreditCards(c2cc);
		
		List<Book> c2b = new ArrayList<Book>();
		c2b.add(mbt.books.get(4));
		c2b.add(mbt.books.get(1));
		c2b.add(mbt.books.get(7));
		customer2.setBooks(c2b);
		
		// CUSTOMER 3
		Customer customer3 = new Customer();
		customer3.setId("2");
		customer3.setFirstName("Thomas");
		customer3.setLastName("Cobal");
		
		customer3.setUsername("t_co");
		customer3.setPassword("password");
		
		List<Address> c3a = new ArrayList<Address>();
		c3a.add(mat.addresses.get(2));
		customer3.setAddresses(c3a);
		
		List<CreditCard> c3cc = new ArrayList<CreditCard>();
		c3cc.add(mcct.creditCards.get(2));
		customer3.setCreditCards(c3cc);
		
		List<Book> c3b = new ArrayList<Book>();
		c3b.add(mbt.books.get(3));
		c3b.add(mbt.books.get(5));
		c3b.add(mbt.books.get(6));
		c3b.add(mbt.books.get(8));
		customer3.setBooks(c3b);
		
		customers.addAll(Arrays.asList(customer1, customer2, customer3));
	}

}
