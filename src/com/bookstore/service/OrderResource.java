package com.bookstore.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import com.bookstore.service.representation.OrderRepresentation;
import com.bookstore.service.workflow.CustomerActivity;
import com.bookstore.service.workflow.OrderActivity;

@CrossOriginResourceSharing(allowAllOrigins = true)

@Path("/order")
public class OrderResource {

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/")
	public List<OrderRepresentation> getOrders(
			@QueryParam("username") String username, 
			@QueryParam("password") String password) {
		OrderActivity orderActivity = new OrderActivity();
		CustomerActivity customerActivity = new CustomerActivity();
		String customerId = customerActivity.authenticate(username, password);
		return orderActivity.getOrders(customerId);
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response getOrder(
			@PathParam("id") String id,
			@QueryParam("username") String username, 
			@QueryParam("password") String password) {
		OrderActivity orderActivity = new OrderActivity();
		CustomerActivity customerActivity = new CustomerActivity();
		// If they're authorized, return the results
		String customerId = customerActivity.authenticate(username, password);
		if (customerId != null) {
			return Response.ok(orderActivity.getOrder(id, customerId)).build();
		}
		// If they aren't authorized, return unauthorized
		return Response.status(Status.UNAUTHORIZED).build();
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/")
	public Response createOrder(
			@QueryParam("username") String username, 
			@QueryParam("password") String password) {
		OrderActivity orderActivity = new OrderActivity();
		CustomerActivity customerActivity = new CustomerActivity();
		// If they're authorized, return the results
		String customerId = customerActivity.authenticate(username, password);
		if (customerId != null) {
			return Response.ok(orderActivity.createOrder(customerId)).build();
		}
		return Response.status(Status.UNAUTHORIZED).build();
	}
	
	// fix this later
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response updateOrder(
			@PathParam("id") String id, 
			@QueryParam("username") String username, 
			@QueryParam("password") String password, 
			OrderRepresentation orderRequest) {
		OrderActivity orderActivity = new OrderActivity();
		CustomerActivity customerActivity = new CustomerActivity();
		// If they're authorized, return the results
		String customerId = customerActivity.authenticate(username, password);
		if (customerId != null) {
			return Response.ok(orderActivity.updateOrder(id, orderRequest.getCustomer(), orderRequest.getBillingAddress(),
					orderRequest.getShippingAddress(), orderRequest.getCreditCard(), orderRequest.getShippingCompany(),
					orderRequest.getLines(), orderRequest.isPaymentReceived(), orderRequest.getOrderState())).build();
		}
		return Response.status(Status.UNAUTHORIZED).build();
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/")
	public Response addBookToOrder( 
			@QueryParam("book_id") String bookId, 
			@QueryParam("username") String username, 
			@QueryParam("password") String password) {
		
		OrderActivity orderActivity = new OrderActivity();
		CustomerActivity customerActivity = new CustomerActivity();
		// If they're authorized, return the results
		String customerId = customerActivity.authenticate(username, password);
		if (customerId != null) {
			return Response.ok(orderActivity.addBookToOrder(customerId, bookId)).build();
		}
		return Response.status(Status.UNAUTHORIZED).build();
	}
	
	@DELETE
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response deleteOrder(@PathParam("id") String id) {
		OrderActivity orderActivity = new OrderActivity();
		String res = orderActivity.deleteOrder(id);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}
}
