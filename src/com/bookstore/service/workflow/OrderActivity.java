package com.bookstore.service.workflow;

import java.util.ArrayList;
import java.util.List;

import com.bookstore.domain.dao.BookDAO;
import com.bookstore.domain.dao.CustomerDAO;
import com.bookstore.domain.dao.OrderDAO;
import com.bookstore.domain.dao.ShippingCompanyDAO;
import com.bookstore.domain.model.Address;
import com.bookstore.domain.model.Book;
import com.bookstore.domain.model.CreditCard;
import com.bookstore.domain.model.Customer;
import com.bookstore.domain.model.Line;
import com.bookstore.domain.model.Link;
import com.bookstore.domain.model.Order;
import com.bookstore.domain.model.ShippingCompany;
import com.bookstore.service.representation.OrderRepresentation;

public class OrderActivity {

	private static OrderDAO orderDao = new OrderDAO();
	private static CustomerDAO customerDao = new CustomerDAO();
	private static BookDAO bookDao = new BookDAO();
	private static ShippingCompanyDAO shippingCompanyDao = new ShippingCompanyDAO();
	
	/**
	 * Translate the order model to a list of order representations.
	 * @return
	 */
	public List<OrderRepresentation> getOrders(String customerId) {
		List<OrderRepresentation> orderRepresentations = new ArrayList<OrderRepresentation>();
		
		if (customerId != null) {
				
			for (Order order : orderDao.getOrdersByCustomerId(customerId)) {
				
				// Add this order to the list of representations.
				orderRepresentations.add(createRepresentation(order));
			}
		}
		return orderRepresentations;
	}
	
	/**
	 * Translates an order model to an order representation.
	 * @param id
	 * @return
	 */
	public OrderRepresentation getOrder(String id, String customerId) {
		Order order = orderDao.getCustomerOrder(id, customerId);
		
		return createRepresentation(order);
	}
	
	/**
	 * Create a new order and return its representation.
	 * @param customer
	 * @param billingAddress
	 * @param shippingAddress
	 * @param creditCard
	 * @param shippingCompany
	 * @param paymentReceived
	 * @return
	 */
	public OrderRepresentation createOrder(String customerId) {
		
		// If an open order exists, send back that one
		//	If it doesn't, send back a new, empty one
		Order order = orderDao.getCustomerCurrentOrder(customerId);
		Customer customer = customerDao.getCustomer(customerId);
		
		if (order == null) {
			order = orderDao.addOrder(customer, customer.getAddresses().get(0), 
					customer.getAddresses().get(0), customer.getCreditCards().get(0),
					shippingCompanyDao.getShippingCompanies().get(0), new ArrayList<Line>(), 
					false, "Open");
		}
		
		return createRepresentation(order);
	}

	/**
	 * UPDATE
	 * @param customer
	 * @param billingAddress
	 * @param shippingAddress
	 * @param creditCard
	 * @param shippingCompany
	 * @param lines
	 * @param paymentReceived
	 * @param orderState
	 * @return 
	 */
	public OrderRepresentation updateOrder(String id, Customer customer, Address billingAddress, Address shippingAddress,
			CreditCard creditCard, ShippingCompany shippingCompany, List<Line> lines, boolean paymentReceived,
			String orderState) {

		// Get the order
		Order order = orderDao.getOrder(id);
		
		// Update the order
		// We can only do these actions in the Open state
		if (order.getOrderState().equals("Open")) {
			order.setCustomer(customer);
			order.setBillingAddress(billingAddress);
			order.setShippingAddress(shippingAddress);
			order.setCreditCard(creditCard);
			order.setShippingCompany(shippingCompany);
			order.setLines(lines);
		}
		
		// We can only do these actions in the Open or Ordered sate
		if (order.getOrderState().equals("Open") || order.getOrderState().equals("Ordered")) {
			order.setPaymentReceived(paymentReceived);
		}
		
		// We can do this in any state, except canceled.
		if (!order.getOrderState().equals("Canceled")) {
			order.setOrderState(orderState);
		}

		return createRepresentation(order);
	}
	
	/**
	 * ADD BOOK TO ORDER
	 * @param id
	 * @param bookId
	 * @return
	 */
	public OrderRepresentation addBookToOrder(String customerId, String bookId) {

		// Get the order
		Order order = orderDao.getCustomerCurrentOrder(customerId);
		
		// Get the book
		Book book = bookDao.getBook(bookId);
		
		// Update the order
		// We can only do these actions in the Open state
		if (order != null) {
			boolean exists = false;
			for (Line line : order.getLines()) {
				if (line.getBook().getId().equals(book.getId())) {
					line.setQuantity(line.getQuantity() +1);
					exists = true;
				}
			}
			if (!exists) {
				order.addBook(book, 1);
			}
		} else {
			Customer customer = customerDao.getCustomer(customerId);
			order = orderDao.addOrder(customer, customer.getAddresses().get(0), 
					customer.getAddresses().get(0), customer.getCreditCards().get(0),
					shippingCompanyDao.getShippingCompanies().get(0), new ArrayList<Line>(), 
					false, "Open");
			order.addBook(book, 1);
		}

		return createRepresentation(order);
	}

	/**
	 * Delete an order based on the passed ID.
	 * @param id
	 * @return
	 */
	public String deleteOrder(String id) {
		orderDao.deleteOrder(id);
		
		return "OK";
	}
	
	/**
	 * Create a representation of an order
	 * @param order
	 * @return
	 */
	private OrderRepresentation createRepresentation(Order order) {
		// Create a representation of the order
		OrderRepresentation orderRep = new OrderRepresentation();
		orderRep.setId(order.getId());
		orderRep.setCustomer(order.getCustomer());
		orderRep.setBillingAddress(order.getBillingAddress());
		orderRep.setShippingAddress(order.getShippingAddress());
		orderRep.setCreditCard(order.getCreditCard());
		orderRep.setShippingCompany(order.getShippingCompany());
		orderRep.setLines(order.getLines());
		orderRep.setPaymentReceived(order.isPaymentReceived());
		orderRep.setOrderState(order.getOrderState());
		setLinks(orderRep);
		return orderRep;
	}
	
	/**
	 * Sets all the links appropriately, for each kind of representation based on state
	 * @param orderRep
	 */
	private void setLinks(OrderRepresentation orderRep) {
		// Set up the activities that can be performed on orders
		Link confirm = new Link("confirm", "http://localhost:8080/COMP433_-_Project_4/services/bookstore/order/" + orderRep.getId());
		Link pay = new Link("pay", "http://localhost:8080/COMP433_-_Project_4/services/bookstore/order/" + orderRep.getId());
		Link ship = new Link("ship", "http://localhost:8080/COMP433_-_Project_4/services/bookstore/order/" + orderRep.getId());
		Link deliver = new Link("deliver", "http://localhost:8080/COMP433_-_Project_4/services/bookstore/order/" + orderRep.getId());
		Link cancel = new Link("cancel", "http://localhost:8080/COMP433_-_Project_4/services/bookstore/order/" + orderRep.getId());
		
		switch (orderRep.getOrderState()) {
		case "Open":
			if (orderRep.isPaymentReceived())
				orderRep.setLinks(confirm, cancel);
			else
				orderRep.setLinks(confirm, pay, cancel);
			break;
		case "Ordered":
			if (orderRep.isPaymentReceived())
				orderRep.setLinks(ship, cancel);
			else 
				orderRep.setLinks(pay, cancel);
			break;
		case "Shipped":
			orderRep.setLinks(deliver);
			break;
		case "Delivered":
		case "Canceled":
			orderRep.setLinks();
			break;
		}
	}
}
