package com.bookstore.service.workflow;

import java.util.ArrayList;
import java.util.List;

import com.bookstore.domain.dao.CustomerDAO;
import com.bookstore.domain.model.Address;
import com.bookstore.domain.model.Book;
import com.bookstore.domain.model.CreditCard;
import com.bookstore.domain.model.Customer;
import com.bookstore.service.representation.CustomerRepresentation;

public class CustomerActivity {
	
	private static CustomerDAO customerDao = new CustomerDAO();
	
	/**
	 * Translates the Customer model to a CustomerRepresentation.
	 * @return
	 */
	public List<CustomerRepresentation> getCustomers() {
		List<CustomerRepresentation> customerRepresentations = new ArrayList<CustomerRepresentation>();
		
		for (Customer customer : customerDao.getCustomers()) {
			// Create a representation of the Book.
			CustomerRepresentation custRep = new CustomerRepresentation();
			custRep.setId(customer.getId());
			custRep.setFirstName(customer.getFirstName());
			custRep.setLastName(customer.getLastName());
			custRep.setAddresses(customer.getAddresses());
			custRep.setCreditCards(customer.getCreditCards());
			custRep.setBooks(customer.getBooks());
			
			// Add it to the list of Book representations.
			customerRepresentations.add(custRep);
		}
		
		return customerRepresentations;
	}
	
	/**
	 * Create a representation of a single customer.
	 * @param id
	 * @return
	 */
	public CustomerRepresentation getCustomer(String id) {
		Customer customer = customerDao.getCustomer(id);
		
		CustomerRepresentation custRep = new CustomerRepresentation();
		custRep.setId(customer.getId());
		custRep.setFirstName(customer.getFirstName());
		custRep.setLastName(customer.getLastName());
		custRep.setAddresses(customer.getAddresses());
		custRep.setCreditCards(customer.getCreditCards());
		custRep.setBooks(customer.getBooks());
		
		return custRep;
	}
	
	/**
	 * Creates a new customer and returns a representation of it.
	 * @param firstName
	 * @param lastName
	 * @param addresses
	 * @param creditCards
	 * @param books
	 * @return
	 */
	public CustomerRepresentation createCustomer(String firstName, String lastName,
												 List<Address> addresses, List<CreditCard> creditCards,
												 List<Book> books) {
		Customer customer = customerDao.addCustomer(firstName, lastName, addresses, creditCards, books);
		
		CustomerRepresentation custRep = new CustomerRepresentation();
		custRep.setId(customer.getId());
		custRep.setFirstName(customer.getFirstName());
		custRep.setLastName(customer.getLastName());
		custRep.setAddresses(customer.getAddresses());
		custRep.setCreditCards(customer.getCreditCards());
		custRep.setBooks(customer.getBooks());
		
		return custRep;
	}
	
	/**
	 * Deletes the customer with the given ID.
	 * @param id
	 * @return
	 */
	public String deleteCustomer(String id) {
		customerDao.deleteCustomer(id);
		
		return "OK";
	}
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public String authenticate(String username, String password) {
		for (Customer customer : customerDao.getCustomers()) {
			if (customer.getUsername().equals(username) && customer.getPassword().equals(password)) {
				return customer.getId();
			}
		}
		return null;
	}
}
