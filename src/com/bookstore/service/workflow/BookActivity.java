package com.bookstore.service.workflow;

import java.util.ArrayList;
import java.util.List;

import com.bookstore.domain.dao.BookDAO;
import com.bookstore.domain.model.Book;
import com.bookstore.domain.model.Link;
import com.bookstore.service.representation.BookRepresentation;

public class BookActivity {

	private static BookDAO bookDao = new BookDAO();
	
	/**
	 * Translates the Book model to a Book representation.
	 * @return
	 */
	public List<BookRepresentation> getBooks() {
		List<BookRepresentation> bookRepresentations = new ArrayList<BookRepresentation>();
		
		for (Book book : bookDao.getBooks()) {
			// Create a representation of the Book.
			BookRepresentation bookRepresentation = new BookRepresentation();
			bookRepresentation.setId(book.getId());
			bookRepresentation.setISBN(book.getISBN());
			bookRepresentation.setTitle(book.getTitle());
			bookRepresentation.setAuthor(book.getAuthor());
			bookRepresentation.setPrice(book.getPrice());
			
			// Add it to the list of Book representations.
			bookRepresentations.add(bookRepresentation);
			
		}
		
		return bookRepresentations;
	}
	
	/**
	 * Create a representation of a single Book.
	 * @param id
	 * @return
	 */
	public BookRepresentation getBook(String id, String orderId) {
		Book book = bookDao.getBook(id);
		
		BookRepresentation bookRepresentation = new BookRepresentation();
		bookRepresentation.setId(book.getId());
		bookRepresentation.setISBN(book.getISBN());
		bookRepresentation.setTitle(book.getTitle());
		bookRepresentation.setAuthor(book.getAuthor());
		bookRepresentation.setPrice(book.getPrice());
		
		// Add the links
		setLinks(bookRepresentation, orderId);
		
		return bookRepresentation;
	}
	
	/**
	 * Creates a new book and returns a representation of that resource.
	 * @param isbn
	 * @param title
	 * @param author
	 * @param price
	 * @return
	 */
	public BookRepresentation createBook(String isbn, String title, String author, double price) {
		Book book = bookDao.addBook(isbn, title, author, price);
		
		BookRepresentation bookRepresentation = new BookRepresentation();
		bookRepresentation.setId(book.getId());
		bookRepresentation.setISBN(book.getISBN());
		bookRepresentation.setTitle(book.getTitle());
		bookRepresentation.setAuthor(book.getAuthor());
		bookRepresentation.setPrice(book.getPrice());
		
		return bookRepresentation;
	}
	
	/**
	 * Deletes a book by ID.
	 * @param id
	 * @return
	 */
	public String deleteBook(String id) {
		bookDao.deleteBook(id);
		
		return "OK";
	}
	
	//
	// Search
	
	/**
	 * Returns books the contain the passed title.
	 * @param title
	 * @return
	 */
	public List<BookRepresentation> getBooksByTitle(String title) {
		List<BookRepresentation> bookRepresentations = new ArrayList<BookRepresentation>();
		
		for (Book book : bookDao.getBooks()) {
			
			if (book.getTitle().toLowerCase().contains(title.toLowerCase())) {
				// Create a representation of the Book.
				BookRepresentation bookRepresentation = new BookRepresentation();
				bookRepresentation.setId(book.getId());
				bookRepresentation.setISBN(book.getISBN());
				bookRepresentation.setTitle(book.getTitle());
				bookRepresentation.setAuthor(book.getAuthor());
				bookRepresentation.setPrice(book.getPrice());
				
				// Add it to the list of Book representations.
				bookRepresentations.add(bookRepresentation);
			}
		}
		
		return bookRepresentations;
	}
	
	/**
	 * Sets all the links appropriately, for each kind of representation based on state
	 * @param orderRep
	 */
	private void setLinks(BookRepresentation bookRep, String orderId) {
		// Set up the activities that can be performed on orders
		Link buy = new Link("buy", "http://localhost:8080/COMP433_-_Project_4/services/bookstore/order?book_id=" + bookRep.getId());
		
		bookRep.setLinks(buy);
	}
}
