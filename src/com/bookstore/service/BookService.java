package com.bookstore.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.core.Response;

import com.bookstore.service.representation.BookRepresentation;
import com.bookstore.service.representation.BookRequest;

@WebService
public interface BookService {

	public List<BookRepresentation> getBooks();
	public BookRepresentation getBook(String id, String orderId);
	public BookRepresentation createBook(BookRequest bookRequest);
	public Response deleteBook(String id);
}
