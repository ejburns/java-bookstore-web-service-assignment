package com.bookstore.service;

import java.util.List;

import javax.ws.rs.core.Response;

import com.bookstore.service.representation.BookRepresentation;
import com.bookstore.service.representation.CustomerRepresentation;
import com.bookstore.service.representation.CustomerRequest;

public interface CustomerService {

	public List<CustomerRepresentation> getCustomers();
	public BookRepresentation getCustomer(String id);
	public BookRepresentation createCustomer(CustomerRequest customerRequest);
	public Response deleteCustomer(String id);
}
