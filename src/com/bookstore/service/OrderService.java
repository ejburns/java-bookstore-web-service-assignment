package com.bookstore.service;

import java.util.List;

import javax.ws.rs.core.Response;

import com.bookstore.service.representation.OrderRepresentation;

public interface OrderService {
	public List<OrderRepresentation> getOrders();
	public OrderRepresentation getOrder(String id);
	public OrderRepresentation createOrder(OrderRepresentation orderRequest);
	public OrderRepresentation updateOrder(String id, OrderRepresentation orderRequest);
	public Response deleteOrder(String id);
}
