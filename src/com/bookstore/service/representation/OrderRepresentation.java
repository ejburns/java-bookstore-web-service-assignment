package com.bookstore.service.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bookstore.domain.model.Address;
import com.bookstore.domain.model.Book;
import com.bookstore.domain.model.CreditCard;
import com.bookstore.domain.model.Customer;
import com.bookstore.domain.model.Line;
import com.bookstore.domain.model.ShippingCompany;

@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderRepresentation extends AbstractRepresentation {
	private String id;
	private Customer customer;
	private Address billingAddress;
	private Address shippingAddress;
	private CreditCard creditCard;
	private ShippingCompany shippingCompany;
	private List<Line> lines = new ArrayList<Line>();
	private boolean paymentReceived;
	private String orderState = "Open";
	
	public OrderRepresentation() {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		if (orderState.equals("Open")) {
			this.customer = customer;
		} else {
			throw new IllegalStateException("Can only add customer in Open state.");
		}
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		if (orderState.equals("Open")) {
			this.billingAddress = billingAddress;
		} else {
			throw new IllegalStateException("Can only add billing address in Open state.");
		}
	}

	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		if (orderState.equals("Open")) {
			this.shippingAddress = shippingAddress;
		} else {
			throw new IllegalStateException("Can only add shipping address in Open state.");
		}
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		if (orderState.equals("Open")) {
			this.creditCard = creditCard;
		} else {
			throw new IllegalStateException("Can only add credit card in Open state.");
		}
	}
	
	public ShippingCompany getShippingCompany() {
		return shippingCompany;
	}

	public void setShippingCompany(ShippingCompany shippingCompany) {
		if (orderState.equals("Open")) {
			this.shippingCompany = shippingCompany;
		} else {
			throw new IllegalStateException("Can only add shipping company in Open state.");
		}
	}

	public List<Line> getLines() {
		return lines;
	}

	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	public boolean isPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(boolean paymentReceived) {
		this.paymentReceived = paymentReceived;
	}
	
	public void addBook(Book book, int quantity) {
		if (orderState.equals("Open")) {
			getLines().add(new Line(book, quantity));
		} else {
			throw new IllegalStateException("Can only add books in Open state.");
		}
	}
	
	public void cancel() {
		if (orderState.equals("Open") || orderState.equals("Ordered")) {
			orderState = "Canceled";
		} else {
			throw new IllegalStateException("Cannot cancel order in this state.");
		}
	}
	
	public void confirmOrder() {
		if (getLines().isEmpty()) {
			orderState = "Canceled";
		} else if (getBillingAddress() == null) {
			throw new IllegalStateException("A billing address is needed before the order can be confirmed.");
		} else if (getShippingAddress() == null) {
			throw new IllegalStateException("A shipping address is needed before the order can be confirmed.");
		} else if (orderState.equals("Open")) {
			orderState = "Ordered";
		} else {
			throw new IllegalStateException("Cannot confirm order in this state.");
		}
	}
	
	public void orderDelivered() {
		if (orderState.equals("Shipped")) {
			orderState = "Delivered";
		} else {
			throw new IllegalStateException("Cannot be delivered from in this state.");
		}
	}
	
	public void orderPayed() {
		if (orderState.equals("Ordered")) {
			setPaymentReceived(true);
		} else {
			throw new IllegalStateException("Cannot pay in this state.");
		}
	}
	
	public void orderSendOut() {
		if (orderState.equals("Ordered") && paymentReceived) {
			orderState = "Shipped";
		} else {
			throw new IllegalStateException("Cannot send out in this state.");
		}
	}
	
	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}
	
	public String getOrderState() {
		return orderState;
	}
	
	public boolean isFinished() {
		if (orderState.equals("Delivered") || orderState.equals("Canceled")) {
			return true;
		}
		return false;
	}
	
	public double getOrderTotal() {
		double total = 0.00;
		for (Line line : lines) {
			total += line.getBook().getPrice() * line.getQuantity();
		}
		return total;
	}
	
}
