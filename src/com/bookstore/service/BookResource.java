package com.bookstore.service;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import com.bookstore.service.representation.BookRepresentation;
import com.bookstore.service.representation.BookRequest;
import com.bookstore.service.workflow.BookActivity;

@CrossOriginResourceSharing(allowAllOrigins = true)

@Path("/book")
public class BookResource implements BookService {
	
	@Override
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/")
	public List<BookRepresentation> getBooks() {
		BookActivity bookActivity = new BookActivity();
		return bookActivity.getBooks();
	}

	@Override
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public BookRepresentation getBook(
			@PathParam("id") String id, 
			@QueryParam("order_id") String orderId) {
		BookActivity bookActivity = new BookActivity();
		return bookActivity.getBook(id, orderId);
	}

	@Override
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/")
	public BookRepresentation createBook(BookRequest bookRequest) {
		BookActivity bookActivity = new BookActivity();
		return bookActivity.createBook(bookRequest.getISBN(), bookRequest.getTitle(), bookRequest.getAuthor(), bookRequest.getPrice());
	}

	@Override
	@DELETE
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response deleteBook(@PathParam("id") String id) {
		BookActivity bookActivity = new BookActivity();
		String res = bookActivity.deleteBook(id);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}

	//
	// Search
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/search/{title}")
	public List<BookRepresentation> getBooks(@PathParam("title") String title) {
		BookActivity bookActivity = new BookActivity();
		return bookActivity.getBooksByTitle(title);
	}
	
}
